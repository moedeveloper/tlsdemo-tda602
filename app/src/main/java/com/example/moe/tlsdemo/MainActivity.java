package com.example.moe.tlsdemo;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class MainActivity extends AppCompatActivity {

    EditText inputPassword=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // setup Strict mode policy
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //final Button button = (Button)findViewById(R.id.btnConnect);
        inputPassword = (EditText)findViewById(R.id.inputPassword);
    }

    public void connectApplication(View view){
        //Toast toat = Toast.makeText(getBaseContext(), inputPassword.getText().toString(), Toast.LENGTH_SHORT);
        //toat.show();

        initiateConnection(inputPassword.getText().toString());
    }

    // Create a trust manager that does not validate certificate chains
    TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
    };

    private void disableSSL(){
        SSLContext sc = null;
        try {
            // Create a trust manager that does not validate certificate chains
            // Install the all-trusting trust manager
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Disable hostname verification
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

    //*
    //
    // Unsecure method to connect to server.
    //
    //
    // *//

    private void initiateConnection(String inputData) {
        InputStream stream = null;
        HttpsURLConnection connection = null;
        DataOutputStream printout;
        try{
            URL url = new URL("https://testbank.nsupdate.info/demo");
            disableSSL();
            connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type","application/json");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches (false);
            //connection.setChunkedStreamingMode(0);
            connection.connect();
            Log.i("MyAPP", "connected..");

            JSONObject jsonParam = new JSONObject();
            jsonParam.put("data", inputData);
            Log.i("MyAPP", jsonParam.toString());

            // Send POST output.
            printout = new DataOutputStream(connection.getOutputStream ());
            printout.writeBytes(jsonParam.toString());
            printout.flush ();
            printout.close ();

            int response = connection.getResponseCode();
            Log.i("MyAPP", "Post Request : " + response);

            //GetRequest(connection, stream);

        } catch (MalformedURLException e){
            e.printStackTrace();
        } catch(IOException ew){
            ew.printStackTrace();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            // Close Stream and disconnect HTTPS connection.
            if (stream != null) {
                Log.i("MyAPP", "stream is closed..");

                try{
                    stream.close();
                }
                catch (IOException ex){
                    ex.printStackTrace();
                }

            }
            if (connection != null) {
                connection.disconnect();
                Log.i("MyAPP", "connection is disconnected..");
            }
        }
    }


    private void PostRequest(HttpsURLConnection connection, String inputData) throws IOException {
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setChunkedStreamingMode(0);


        OutputStream out = new BufferedOutputStream(connection.getOutputStream());
        out.write(inputData.getBytes());
        out.flush();
        out.close();
    }

    private void setupGetRequest(HttpsURLConnection connection) throws ProtocolException {

        // Timeout for reading InputStream arbitrarily set to 3000ms.
        connection.setReadTimeout(3000);
        // Timeout for connection.connect() arbitrarily set to 3000ms.
        connection.setConnectTimeout(3000);
        // For this use case, set HTTP method to GET.
        connection.setRequestMethod("GET");
        // Already true by default but setting just in case; needs to be true since this request
        // is carrying an input (response) body.
        //connection.setDoInput(true);
        // Open communications link (network traffic occurs here).
    }

    private void GetRequest(HttpsURLConnection connection, InputStream stream) throws IOException {

        String result = null;


        int responseCode = connection.getResponseCode();
        if (responseCode != HttpsURLConnection.HTTP_OK) {
            throw new IOException("HTTP error code: " + responseCode);
        }
        // Retrieve the response body as an InputStream.
        stream = connection.getInputStream();
        Log.i("MyAPP", "Stream on..");

        if (stream != null) {
            // Converts Stream to String with max length of 500.
            result = readStream(stream, 500);
            Log.i("MyAPP", result);
        }
    }

    /**
     * Converts the contents of an InputStream to a String.
     */
    private String readStream(InputStream stream, int maxLength) throws IOException {
        String result = null;
        // Read InputStream using the UTF-8 charset.
        InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
        // Create temporary buffer to hold Stream data with specified max length.
        char[] buffer = new char[maxLength];
        // Populate temporary buffer with Stream data.
        int numChars = 0;
        int readSize = 0;
        while (numChars < maxLength && readSize != -1) {
            numChars += readSize;
            int pct = (100 * numChars) / maxLength;
            readSize = reader.read(buffer, numChars, buffer.length - numChars);
        }
        if (numChars != -1) {
            // The stream was not empty.
            // Create String that is actual length of response body if actual length was less than
            // max length.
            numChars = Math.min(numChars, maxLength);
            result = new String(buffer, 0, numChars);
        }
        return result;
    }

}
