﻿
/*
const express = require('express');

// Constants
const PORT = 4000;

// App
const app = express();
app.get('/', function (req, res) {
  res.send('Hello world\n');
});

app.listen(PORT);
console.log('Running on http://localhost:' + PORT);
*/

'use strict';

var http = require('http');
var express = require("express");
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next){
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});


var server = http.createServer(app);

app.use('/demo', function(req, res, next){
    console.log(req.body.data);

    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end('post received');
    //next();
});


// handle error
app.use(function(err, req, res, next) {
	if (err) {
		res.status(err.status || 400).send(
		{
			message: err.message
		});
	}
	//res.header("Content-Type", "application/json; charset=utf-8");
	next();
});

server.listen(4000, function() {

	console.log('server running!');
});

module.exports = app;